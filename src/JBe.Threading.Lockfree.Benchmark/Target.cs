﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JBe.Threading.Benchmark
{
    internal class Target
    {
        private double value;
        private readonly object syncRoot = new object();

        public double Value
        {
            get
            {
                lock (syncRoot) return value;
            }
        }

        internal void Update(int p)
        {
            lock (syncRoot) value = p;
        }
    }
}
