﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JBe.Threading.Benchmark
{
    internal class TargetLockfree
    {
        private double value;
        private readonly object syncRoot = new object();

        public double Value
        {
            get
            {
                return JBe.Threading.Lockfree.Read(ref value);
            }
        }

        internal void Update(int p)
        {
            Lockfree.Update(ref value, p);
        }
    }
}
