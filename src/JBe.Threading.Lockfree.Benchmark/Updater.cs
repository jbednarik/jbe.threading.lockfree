﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JBe.Threading.Benchmark
{
    static class Updater
    {
        public static Result UpdateTarget(Target target, CancellationToken token)
        {
            var result = new Result { TaskRunningWaitHandle = new ManualResetEventSlim(false) };
            result.Task = Task.Factory.StartNew(() =>
            {
                result.TaskRunningWaitHandle.Set();

                while (!token.IsCancellationRequested)
                {
                    target.Update(1);
                    Interlocked.Increment(ref result.WriteCycles);
                }
            });
            return result;
        }

        public static Result UpdateTarget(TargetLockfree target, CancellationToken token)
        {
            var result = new Result { TaskRunningWaitHandle = new ManualResetEventSlim(false) };
            result.Task = Task.Factory.StartNew(() =>
            {
                result.TaskRunningWaitHandle.Set();

                while (!token.IsCancellationRequested)
                {
                    target.Update(1);
                    Interlocked.Increment(ref result.WriteCycles);
                }
            });
            return result;
        }
    }
}
