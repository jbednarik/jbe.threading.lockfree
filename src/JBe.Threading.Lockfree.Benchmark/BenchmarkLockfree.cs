﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JBe.Threading.Benchmark
{
    class BenchmarkLockfree
    {
        public static TimeSpan Run()
        {
            TimeSpan elapsed = new TimeSpan();

            TargetLockfree target = new TargetLockfree();

            CancellationTokenSource cts = new CancellationTokenSource();

            Result[] results = new Result[Program.threads];

            var sw = Stopwatch.StartNew();
            for (int i = 0; i < Program.threads; i++)
            {
                results[i] = Updater.UpdateTarget(target, cts.Token);
            }

            WaitHandle.WaitAll(results.Select(x => x.TaskRunningWaitHandle.WaitHandle).ToArray());

            double a = 0;
            for (int i = 0; i < Program.readIterations; i++)
            {
                a = target.Value;
            }
            elapsed = sw.Elapsed;
            cts.Cancel();

            Task.WaitAll(results.Select(x => x.Task).ToArray());

            return elapsed;
        }
    }
}
