﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JBe.Threading.Benchmark
{
    class Program
    {
        internal const int threads = 8;
        internal const int readIterations = 100000000;

        static void Main(string[] args)
        {
            TimeSpan elapsed;
            Console.Write("Running bechmark with locking: ");
            elapsed = BenchmarkUsingLocking.Run();
            Console.WriteLine(elapsed);

            Console.Write("Running bechmark with lock free: ");
            elapsed = BenchmarkLockfree.Run();
            Console.WriteLine(elapsed);

            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}
