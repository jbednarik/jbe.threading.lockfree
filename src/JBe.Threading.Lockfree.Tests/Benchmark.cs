﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Linq;

namespace JBe.Threading.Tests
{
    [TestClass]
    public class Benchmark
    {
        [TestMethod]
        public void BenchmarkUsingLocking()
        {
            TimeSpan elapsed = new TimeSpan();

            Thread thread = new Thread(() =>
            {
                const int threads = 8;
                const int readIterations = 100000000;

                Target target = new Target();

                CancellationTokenSource cts = new CancellationTokenSource();

                Result[] results = new Result[threads];

                var sw = Stopwatch.StartNew();
                for (int i = 0; i < threads; i++)
                {
                    results[i] = StartUpdatingThread(cts.Token, target);
                }

                WaitHandle.WaitAll(results.Select(x => x.TaskRunningWaitHandle.WaitHandle).ToArray());

                double a = 0;
                for (int i = 0; i < readIterations; i++)
                {
                    a = target.Value;
                }
                elapsed = sw.Elapsed;
                Trace.WriteLine(a);
                cts.Cancel();

                Task.WaitAll(results.Select(x => x.Task).ToArray());
            });
            thread.SetApartmentState(ApartmentState.MTA);
            thread.Start();
            thread.Join();
            Assert.Inconclusive("It took: " + elapsed);
        }

        [TestMethod]
        public void BenchmarkLockfree()
        {
            TimeSpan elapsed = new TimeSpan();

            Thread thread = new Thread(() =>
            {
                const int threads = 8;
                const int readIterations = 100000000;

                TargetLockfree target = new TargetLockfree();

                CancellationTokenSource cts = new CancellationTokenSource();

                Result[] results = new Result[threads];

                var sw = Stopwatch.StartNew();
                for (int i = 0; i < threads; i++)
                {
                    results[i] = StartUpdatingThread(cts.Token, target);
                }

                WaitHandle.WaitAll(results.Select(x => x.TaskRunningWaitHandle.WaitHandle).ToArray());

                double a = 0;
                for (int i = 0; i < readIterations; i++)
                {
                    a = target.Value;
                }
                elapsed = sw.Elapsed;
                Trace.WriteLine(a);
                cts.Cancel();

                Task.WaitAll(results.Select(x => x.Task).ToArray());
            });
            thread.SetApartmentState(ApartmentState.MTA);
            thread.Start();
            thread.Join();
            Assert.Inconclusive("It took: " + elapsed);
        }

        

        private static Result StartUpdatingThread(CancellationToken token, TargetLockfree target)
        {
            var result = new Result { TaskRunningWaitHandle = new ManualResetEventSlim(false) };
            result.Task = Task.Factory.StartNew(() =>
            {
                result.TaskRunningWaitHandle.Set();

                while (!token.IsCancellationRequested)
                {
                    target.Update(1);
                    Interlocked.Increment(ref result.WriteCycles);
                }
            });
            return result;
        }

        private class Target
        {
            private double value;
            private readonly object syncRoot = new object();

            public double Value
            {
                get
                {
                    lock (syncRoot) return value;
                }
            }

            internal void Update(int p)
            {
                lock (syncRoot) value = p;
            }
        }

        private class TargetLockfree
        {
            private double value;
            private readonly object syncRoot = new object();

            public double Value
            {
                get
                {
                    return JBe.Threading.Lockfree.Read(ref value);
                }
            }

            internal void Update(int p)
            {
                Lockfree.Update(ref value, p);
            }
        }

        class Result
        {
            public ManualResetEventSlim TaskRunningWaitHandle;
            public int WriteCycles;
            public Task Task;
        }
    }
}
