﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace JBe.Threading
{
    public static class Lockfree
    {
        public static void Update(ref double value, double newValue)
        {
            double initialValue = value;
            if (Interlocked.CompareExchange(ref value, newValue, initialValue) != initialValue)
            {
                var spinner = new SpinWait();
                do
                {
                    spinner.SpinOnce();
                    initialValue = value;
                } while (Interlocked.CompareExchange(ref value, newValue, initialValue) != initialValue);
            }
        }

        public static void Update(ref double value, Func<double, double> updateFunction)
        {
            double initialValue = value;
            if (Interlocked.CompareExchange(ref value, updateFunction(initialValue), initialValue) != initialValue)
            {
                var spinner = new SpinWait();
                do
                {
                    spinner.SpinOnce();
                    initialValue = value;
                } while (Interlocked.CompareExchange(ref value, updateFunction(initialValue), initialValue) != initialValue);
            }
        }

        public static double Read(ref double value)
        {
            return Interlocked.CompareExchange(ref value, value, -value);
        }
    }
}
